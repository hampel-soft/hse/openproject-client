# OpenProject API for LabVIEW

The VIs in this repository are created and provided by HAMPEL SOFTWARE ENGINEERING (HSE, www.hampel-soft.com).

This repository hosts an OpenProject client application and an API to OpenProject, both written in and for NI LabVIEW(tm).

To try and use this sample program, you need an OpenProject installation and a valid API key. Copy the /LV2016/rest.ini.default file to /LV2016/rest.ini and fill in URL and API key.

## Contact

Please get in touch with us at (office@hampel-soft.com) or visit our website (www.hampel-soft.com)
